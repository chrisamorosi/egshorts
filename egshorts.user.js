// ==UserScript==
// @name         Shortcuts for Evergreen
// @version      1.1.6
// @description  Adds keyboard shortcuts to the Evergreen web client for cataloging.
// @author       Chris Amorosi
// @homepageURL  https://gitlab.com/chrisamorosi/egshorts/
// @match        http*://*/eg*/staff/*
// @match        http*://*/eg*/opac/*
// @updateURL    https://gitlab.com/chrisamorosi/egshorts/-/raw/master/egshorts.user.js
// @downloadURL  https://gitlab.com/chrisamorosi/egshorts/-/raw/master/egshorts.user.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/mousetrap/1.6.3/mousetrap.min.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/mousetrap/1.6.3/plugins/global-bind/mousetrap-global-bind.min.js
// ==/UserScript==

(function() {
    'use strict';

    var Mousetrap = window.Mousetrap; // This quiets an annoying error.

    // Item adding shortcuts

    Mousetrap.bindGlobal('alt+a', function() {
        // This works but only when it's done twice for some reason.
        // The focus needs to escape the iframe that typically gets focuses.

        top.focus();
        const add = document.getElementById("add-holdings-btn");
        add.click();

    });

    Mousetrap.bindGlobal('alt+.', function() {
        document.getElementById("barcode-input--2").focus();
    });

    Mousetrap.bindGlobal('alt+y', function() {
        // Apply copy template

        const apply = document.querySelector("#ngb-nav-0-panel > div:nth-child(2) > eg-copy-attrs > div.row.border.rounded.border-dark.pt-2.pb-2.bg-faint > div.col-lg-7.d-flex > button");

        apply.click();
        const callnumber = document.querySelector("#ngb-nav-0-panel > div:nth-child(1) > eg-vol-edit > div.row.d-flex.mt-1.vol-row > div:nth-child(4) > input");
        console.log(callnumber);
        callnumber.focus();
        callnumber.select();
    });

    Mousetrap.bindGlobal('alt+t', function() {
        // Focus template search box

        const templateSelect = document.getElementById("template-select");
        templateSelect.focus();
        templateSelect.select();
    });

    Mousetrap.bindGlobal('alt+c', function() {
        // Save items

        const saveItem = document.querySelector("#staff-content-container > ng-component > div.row.m-2.p-2.border.border-dark.rounded.bg-faint > div > button.btn.btn-outline-dark.ms-2");
        saveItem.click();
    });

    Mousetrap.bindGlobal('alt+p', function() {
        // Edit price of item
        // Not updated for Angular yet.

        var price = document.querySelector('input[ng-model][ng-model="working.price"]');
        price.focus();
        price.select();
    });

    // Serials inputting shortcuts

    Mousetrap.bindGlobal('alt+l', function() {
        // Serials quick receive
        // Not updated for Angular yet.

        document.querySelector('a[ng-click][ng-click="quickReceive()"]').click();
    });

    // Item Status barcode helper
    // Allows copying and pasting a column of barcodes into the Item Status box
    // by replacing spaces with commas.

    Mousetrap.bindGlobal('alt+b', function() {
        const submitBox = document.getElementById('item-status-barcode');
        submitBox.value = submitBox.value.replaceAll(' ',',');
    });

    /*

    // Add item (for multiple items on the same record).
    Mousetrap.bindGlobal('+', function() {
        let addItem = document.querySelector('button[title="Add Item"]');
        addItem.click();
    });

    // Remove item (opposite of add above).
    
    Mousetrap.bindGlobal('-', function() {
        let removeItem = document.querySelector('button[title="Remove Item"]');
        removeItem.click();
    });

    // Opens a New MARC page (not consistent) and clicks Load when on the New MARC page.

    Mousetrap.bindGlobal('alt+m', function() {
        const newMarc = GM_addElement('a', {
            href: '/eg/staff/cat/catalog/new_bib'
        });
        newMarc.click();
        document.querySelector('button[ng-click][ng-click="loadTemplate()"]').click();
    });
    */

})();