# egshorts - Shortcuts for Evergreen

## Description

This userscript adds cataloging-centric keyboard shortcuts to the Evergreen ILS web client using the [Mousetrap](https://craig.is/killing/mice) JavaScript library.

Note: I only know that this works on my consortium's installation of Evergreen 3.4.

## Pre-Installation

First you will need to install a browser extension called a userscript manager. I use [Tampermonkey on Chrome](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo//Open).

You should then set up your catalog as a search engine in your browser, ideally as your primary one. To do this in Chrome:

1. Open the three-dot menu and click **Settings**.
2. Scroll to the end. You should see a **Search engine** section. Click **Manage search engines** underneath.
3. Click **Add**.
4. A popup will open. In **Search engine** type something like "My catalog". In **Keyword** type something short, like "c". In **URL with %s in place of query** enter `https://www.your-catalog.org/eg/staff/cat/catalog/results?query=%s` changing the URL to your catalog's domain.
5. Click **Add**.
6. Optional: Make the catalog your default search engine by clicking the three-dot icon next to its entry in the list and picking **Make default**.

## Installation

Click on [this link](https://gitlab.com/chrisamorosi/shortcutsforevergreen/-/raw/master/egshorts.user.js). Then click the **Install** button. The script should now be active in the next Evergreen tab you open!

## The Shortcuts

### Focus Address Bar: Alt + D

This is actually a common browser shortcut, not from my script.

Use this to focus the address bar, which you will use to search the catalog. If you made your catalog the default search engine, scan an ISBN to search for a record. If you didn't make it your default, type `c` then `Tab` to set the engine to your catalog and then scan an ISBN.

### Add item: Alt + A

Triggers the Add Holdings button on a bib record page. For some reason, you may have to type it twice.

### Apply template: Alt + Y

Applies the active item template to the working item. It will also helpfully move your cursor to the call number field so you can edit it immediately.

### Edit price: Alt + P

Focuses and highlights the price field for easy editing.

### Save item: Alt + C

Saves the item(s) you're working on.

### Receive serials: Alt + L

Triggers the Quick Receive popup for serials.


## Bugs

### Double-tapping Alt + A

For some reason, you may have to key in Alt + A twice to trigger item adding.

### Shortcuts stop working

Sometimes the script will lose focus and stop working. Click in the barcode or call number field and then try the shortcut again.
